package lab5;
/**
 * Receive 8-bit strings for representing chars.
 * 
 * @author Leilani Hagen
 * @date May 25, 2018
 *
 */
public class Receive {
	
	public static int HIGH = 1;
	public static int LOW = 0;
	public static int WAIT = 1000; // num. milliseconds to wait between each clock line raise.
	
	private int clock = LOW;
	private int data = HIGH;
	private int charNum = 0; // Will count up to 8 (bits) for each new char...
	private int[] bits;

	public static void main(String[] args) {
		
		while (true) {
			if (charNum == 8) { // One whole char read...
				charNum = 0;
				// parse the bitstring in bits[] and print it to syso...
			}
			
			clock = HIGH;
			if (data == HIGH ) {
				bits[charNum] = 1; // Write a 1...
			}
			else if (data == LOW) {
				bits[charNum] = 0; // Write a 0...
			}
			charNum++;
			clock = LOW;
			Thread.sleep(WAIT);
		}
	}

}
