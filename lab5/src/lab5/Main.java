package lab5;

import java.util.Scanner;
import com.pi4j.io.gpio.*;

/**
 * Receive 8-bit strings for representing chars.
 * 
 * @author: Leilani Hagen, Pierson Carulli, Anais Barja.
 * @date May 25, 2018
 * 
 */
public class Main {

	public static int HIGH = 1;
	public static int LOW = 0;
	public static int WAIT = 1000; // num. milliseconds to wait between each clock line raise.

	private int clock = LOW;
	private int data = HIGH;
	private int charNum = 0; // Will count up to 8 (bits) for each new char...
	private int[] bits;

	public static void main(String[] args) {

//		// Set up clock pin controller:
//		GpioController gpio = GpioFactory.getInstance();
//		GpioPinDigitalOutput clockPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04); // pin 16
//		// Gpio.pullUpDnControl(4, Gpio.PUD_UP);
//
//		// Set up data pin controller:
//		// GpioController gpio = GpioFactory.getInstance();
//		GpioPinDigitalOutput dataPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05); // pin 18
//		// Gpio.pullUpDnControl(5, Gpio.PUD_UP);

		// Get a Scanner on System.in:
		Scanner input = new Scanner(System.in);

		// Loop:
		while (input.hasNext()) {

			// Get a single line of input:
			String data = input.next();

			// Create Sender object:
			SenderUtil s = new SenderUtil();

			// Generate the bitStrings[] and backwardsBS[] arrays:
			s.buildBitStrings(data);
			String[] backwardsBS = s.reverseBS();

			// Send each char in the current token:
			for (String bs : backwardsBS) {
				for (int i = 0; i < 8; i++) { // blowing up
					if (bs.charAt(i) == '0') { // Send a 0...

						// Send a 0:
//						dataPin.low();
					} else if (bs.charAt(i) == '1') {

						// Send a 1:
//						dataPin.high();
					}

					// Pulse the clock line:
//					clockPin.high();
					try {
						Thread.sleep(WAIT);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

//					clockPin.low();
				}

				// Test:
				for (int i = 0; i < s.getBsLength(); i++) {
					System.out.println("Element " + i);
					System.out.println(s.getBitStrings()[i]);
					System.out.println(backwardsBS[i]);
					System.out.println();
				}
			}
		}

	}
}
