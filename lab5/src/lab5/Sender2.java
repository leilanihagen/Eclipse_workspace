package lab5;

public class Sender2 {
	
	public String[] bitStrings;
	
	public void buildBsArray(String data) {
		/*
		 * Accepts data string, converts data to chars, and converts each char to a bitstring.
		 * 
		 * @return String[] containing bitstring representations of each character in input.
		 */
		
		// Get a bitStrings array:
		char[] dataChars = data.toCharArray();
		
		// Convert characters to bit strings: 
		String[] bitStrings = new String[dataChars.length];
		int bsCount = 0;
		int dcInt;
		for (char dc: dataChars) {
			System.out.println(dc);
			dcInt = (int)dc;
				bitStrings[bsCount] = Integer.toBinaryString(dcInt);
				bsCount++;
		}
		
		// Append truncated zeros to the beginning of the bs:
		for (String bs: bitStrings) {
			int bitCount = 0; // Bits in this char...
			char[] untruncatedString = new char[8]; // For this character....
			int runningLength = bs.length(); // Running len of the untruncated string, as 0s are added.
			
			// Add zeros:
			while (bs.charAt(bitCount) == '1' && runningLength < 8) { // Add zeros.
				untruncatedString[bitCount] = '0';
				bitCount++;
				runningLength++;
			} // all necessary zeros have been added to the beginning...
			
			// Append the rest of the old bs String to our uncruncatedString:
			for (int j=0; bitCount<8; bitCount++) {
				untruncatedString[bitCount] = bs.charAt(j++);
			}
			
			// Overwrite bitStrings array with untruncated bit strings:
			String temp = null;
			for (int i = 0; i < bsCount; i++) {
				// Convert untruncatedString char array to String:
				for (int p=0; p<8; p++) {
					temp = new String(untruncatedString);
					System.out.println(temp);
				}
				bitStrings[i] = temp;
			}
		}

		this.bitStrings = bitStrings;
	}
	
	public String[] reverseBS() {
		/*
		 * Reverse the order of the bits in each bit string in the bsArray.
		 */
		
		String[] backwardsBS = new String[8];
		
		char[] reversedBits = new char[8];
		String temp = null;
		int bsCount = 0;
		for (String bs: bitStrings) {
			
			for (int i=7, j=0; i>=0; i--, j++) {
				reversedBits[j] = bs.charAt(i);
			}
			backwardsBS[bsCount] = new String(reversedBits); 
			bsCount++;
		}
		
		return backwardsBS; 
	}
}
