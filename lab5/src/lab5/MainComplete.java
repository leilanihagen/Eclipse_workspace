package lab5;

import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;

/**
 * Receive 8-bit strings for representing chars.
 * 
 * @author Leilani Hagen
 * @date May 25, 2018
 *
 */
public class MainComplete {
	
	public static int HIGH = 1;
	public static int LOW = 0;
	public static int WAIT = 1000; // num. milliseconds to wait between each clock line raise.
	
	private int clock = LOW;
	private int data = HIGH;
	private int charNum = 0; // Will count up to 8 (bits) for each new char...
	private int[] bits;

	public static void main(String[] args) {
		
		// Get a Scanner of System.in for the data to send:
		Scanner input = new Scanner(System.in);
		String data = input.nextLine(); // Get a single line of input...
		String testBS = null;
		
		// Get a bitStrings array:
		char[] dataChars = data.toCharArray();
		
		// Convert characters to bit strings: 
		String[] bitStrings = new String[dataChars.length];
		int bsCount = 0;
		int dcInt;
		for (char dc: dataChars) {
			System.out.println(dc);
			dcInt = (int)dc;
				bitStrings[bsCount] = Integer.toBinaryString(dcInt);
				bsCount++;
		}
		
		// Append truncated zeros to the beginning of the bs:
		for (String bs: bitStrings) {
			int bitCount = 0; // Bits in this char...
			char[] untruncatedString = new char[8]; // For this character....
			int runningLength = bs.length(); // Running len of the untruncated string, as 0s are added.
			
			// Add zeros:
			while (bs.charAt(bitCount) == '1' && runningLength < 8) { // Add zeros.
				untruncatedString[bitCount] = '0';
				bitCount++;
				runningLength++;
			} // all necessary zeros have been added to the beginning...
			
			// Append the rest of the old bs String to our uncruncatedString:
			for (int j=0; bitCount<8; bitCount++) {
				untruncatedString[bitCount] = bs.charAt(j++);
			}
			
			for (int i=0; i<8; i++) {
				//System.out.print(untruncatedString[i]);
				//////
				if(testBS==null) {
					testBS=(untruncatedString[i]+"");
				}
				else {
					
					testBS=(testBS + untruncatedString[i]);
				}
			}
			//System.out.print(" ");
//			runningLength =0;
		}
			
		System.out.println(testBS);
		
		int x=0;
		String newBS=null;
		
		while(x<testBS.length()) {
			if(newBS==null) {
				newBS=(testBS.charAt(x)+"");
			}
			else {
				newBS=(testBS.charAt(x) + newBS);
			}
			x++;
		}
		System.out.println(newBS);
//		System.out.println(bitStrings[0]+" "+bitStrings[1]+" "+bitStrings[2]+" "+bitStrings[3]+" "+bitStrings[4]);
		
//		// Send the data one character at a time:
//		for (String charBitStr: bitStrings) {
//			System.out.println(charBitStr);
//			char[] bitArray = charBitStr.toCharArray();
//
//			System.out.println("Original bitstring: " + bitArray.toString());
//			
//			// Reverse bit order:
//			char[] reverseBitArray = new char[8];
//			int counter = 8;
//			for (int i=0; i<8; i++) {
//				reverseBitArray[i] = bitArray[counter];
//				counter -= 1;
//			}
//			System.out.println("Reversed bitstring: " + reverseBitArray.toString());
//		}
	}

}
