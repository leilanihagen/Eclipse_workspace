import com.pi4j.io.gpio.*;
import java.util.Scanner;
import com.pi4j.wiringpi.*;

public class Button2B {
	public static void main(String[] args) {
		
		GpioController gpio = GpioFactory.getInstance();
		GpioPinDigitalInput pin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_05); // Data line/pin 18
		Gpio.pullUpDnControl(5, Gpio.PUD_UP); // was 4
		int count = -1;
		boolean hasbeen_unpressed = true; // start the state out as one

		long wait = 0;
		long howmanymilli = 210;

		while (true) {
			if (pin.getState() == PinState.HIGH) {
				if (hasbeen_unpressed == true) {

					wait = (System.currentTimeMillis() + howmanymilli);

					count++; // increment the count
					hasbeen_unpressed = false; // prevent infinite loop
					System.out.println("pressed: " + count + " times");
				}
			} else {
				while (System.currentTimeMillis() < wait) {
					// ?
				}
				hasbeen_unpressed = true; // Allow count to be incremented again
			}
		}
	}
}
