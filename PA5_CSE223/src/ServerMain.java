import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Server chat application!
 *
 * The server application works by creating two Runnable objects, an SSender to send keyboard
 * input (messages) from the server application user to a client, and an SReceiver to read
 * input messages sent from the client. The SSender Runnable is instantiated once, and passed
 * to a Thread object that runs for the life of the program. The SSender captures input from
 * System.in continuously and sends it to the current client. The SReceiver, on the other hand,
 * is re-instantiated for every new client that connects. The new instance is passed the new
 * Socket object for the new client, and receives messages from this client. The server listens
 * for connections on port 1201 and will open the chat dialog for anyone who connects. When the
 * client disconnects, a new client can join and a chat will be opened with this client.
 *
 * Additionally, the server user is prompted to enter a username, which will be saved and used
 * for the life of the program and in dialog with any client that the user chats with.
 *
 * @author Leilani Hagen
 * @date 2018-06-10
 * @assignment PA5 - CSE223
 */
public class ServerMain {

    public static class OutputSocket {
        /*
        Create our own wrapper around a Socket object that can be changed during running main,
        so that keyboard input from the server user can be sent to a dynamic socket. An
        OutputSocket is passed to SSender, and, using OutputSocket's setter, the specific
        OutputSocket's socket can be changed, switching the actual socket that the SSender
        writes keyboard input from the server user to.
         */

        private Socket currentSocket;

        public OutputSocket(Socket socket) {
            /*
            Constructor.
             */

            currentSocket = socket;
        }

        public void setCurrentSocket(Socket socket) {
            /*
            Reset the currentSocket.
             */

            currentSocket = socket;
        }

        public void sendText(String string) {
            /*
            Send a line of text to the current output socket's output stream.
             */

            if (currentSocket != null) {
                PrintWriter pw = null;
                try {
                    pw = new PrintWriter(currentSocket.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                pw.println(string);
                pw.flush();
            } else {
                System.out.println("Ignored text because no current connection: <" + string + ">");
            }
        }
    }

    public static void main(String[] args) {

        boolean firstClient = true;

        greetUser();
        String username = getUsername();

        // Create the server socket:
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(1_201);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create an output socket:
        OutputSocket outputSocket = new OutputSocket(null);

        // Get a Scanner on input from the server user:
        Scanner input = new Scanner(System.in);

        // Create a single SSender:
        SSender sender = new SSender(outputSocket, input);
        Thread sThread = new Thread(sender);
        sThread.start();

        while (true) {

            // Display reconnection dialog to subsequent client requests after the first client has left:
            System.out.println("Waiting for a connection to client...");

            // Wait for a connection from client:
            Socket socket = null;
            try {
                socket = ss.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            outputSocket.setCurrentSocket(socket);

            // Send our username to the client:
            PrintWriter nameSender = null;
            try {
                nameSender = new PrintWriter(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            nameSender.println(username);
            nameSender.flush();

            // Store the client's username:
            Scanner captureName = null;
            try {
                captureName = new Scanner(socket.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            String clientUsername = captureName.next();

            // Connection has been established at this point.... decide if new connection or initial connection:
            if (firstClient) {
                // Display first client connection dialog:
                System.out.println("Client connection established! You may now chat with " + clientUsername + "!");
                System.out.println();
                firstClient = false; // Only run once...
            } else {
                // Display subsequent client connection dialog:
                System.out.println();
                System.out.println("New client connected! You may now chat with " + clientUsername + "!");
                System.out.println();
            }

            // Create our Runnable:
            SReceiver receiver = new SReceiver(socket, clientUsername);

            // Thread our Receiver:
            Thread rThread = new Thread(receiver);

            // Start the thread:
            rThread.start();

            // Wait until the receiving thread dies to try to connect to a new client:
            while (rThread.isAlive()){
                try {
                Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            outputSocket.setCurrentSocket(null);
        }
    }

    private static void greetUser() {
        /*
        Greet the server user and give them some instructions.
         */

        System.out.println();
        System.out.println("************************************************************************");
        System.out.println("Hello, welcome to your chat server application!");
        System.out.println();
        System.out.println("Your program will wait for a connection request from a client, and");
        System.out.println("when one is received, you will be able to chat with the client!");
        System.out.println();
        System.out.println("The client may exit at any time. However, if the client exits, you can");
        System.out.println("wait for connection requests from a new client.");
        System.out.println();
        System.out.println("If you wish to exit, press CTRL + C");
        System.out.println("************************************************************************");
        System.out.println();
    }

    private static String getUsername() {
        /*
        Get a username from the server, so that the client knows who they're chatting with!
         */

        Scanner captureName = new Scanner(System.in);
        System.out.print("Please enter a username for this chat session: ");
        String name = captureName.next();
        System.out.println();
        System.out.println("Hello " + name + "!");
        System.out.println();
        return name;
    }
}