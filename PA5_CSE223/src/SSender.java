import java.util.Scanner;

/**
 * Server-side message sender. The SSender instance should be passed to a single thread that will stay
 * alive for the entire life of the server application. The SSender's job is simply to send new lines
 * of input from a Scanner into a specified OutputSocket (see ServerMain).
 *
 * @date 2018-06-10
 * @author Leilani Hagen
 * @assignment PA5 - CSE223
 */
public class SSender implements Runnable {

    private ServerMain.OutputSocket outputSocket;
    private Scanner scanner;

    public SSender(ServerMain.OutputSocket outputSocket, Scanner scanner) {
        /*
        Mandatory constructor.
         */

        this.outputSocket = outputSocket;
        this.scanner = scanner;
    }

    @Override
    public void run() {
        /*
        Send-messages loop.
         */

        // While new messages, send them through the socket:
        while (scanner.hasNextLine()){

            outputSocket.sendText(scanner.nextLine());
        }
    }
}