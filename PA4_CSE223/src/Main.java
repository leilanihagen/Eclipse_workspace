import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Main class for a command-line interface to the 20+ questions game.
 * 
 * @author Leilani Hagen
 * @date May 26, 2018
 * @assignment PA4 - CSE223
 * 
 */
public class Main {

	public static String database = "test.txt";

	public static void main(String args[]) {
		/* main method... */

		File dbFile = new File(database);
		
		
		// Create a DecisionTree instance:
		DecisionTree brain = new DecisionTree();

		// Ingest from the file into our brain:
		Scanner dbScanner;
		try {
			dbScanner = new Scanner(dbFile);
		} catch (FileNotFoundException e) {
			System.out.println("Please make sure the database file exists and is properly"
					+ " initialized with at least one question node before attempting to play.");
			e.printStackTrace();
			return;
		}
		brain.ingest(dbScanner);
		dbScanner.close();

		// Welcome:
		System.out.println("Welcome to 20+ (or ++) questions!");
		System.out.println();
		startupDialog();
		
		// Play the game:
		brain.playGame();
		
		// Serialize our new, potentially modified DecisionTree brain back to file:
		PrintWriter dbWriter;
		try {
			dbWriter = new PrintWriter(dbFile);
		} catch (FileNotFoundException e) {
			System.out.println("Please make sure the database file exists and is properly"
					+ " initialized with at least one question node before attempting to play.");
			e.printStackTrace();
			return;
		}
		brain.serialize(dbWriter);
		dbWriter.close();
	}

	private static void startupDialog() {
		/* All the print statements that run at the beginning of the game instructing the user on
		 * how to play...
		 */
		
		// Instructions:
		System.out.println("Instructions:");
		System.out.println("Think of something - anything. The AI will ask a series of questions");
		System.out.println("until it reaches it's final guess, then it will tell you it's best");
		System.out.println("guess based on the answers you gave. If it answers correctly, the AI");
		System.out.println("wins.");
		System.out.println();
		System.out.println("If the AI guesses worng, you win, but as the proven smarter agent,");
		System.out.println("you get to teach the AI a lesson on how to play like a real pro...");
		System.out.println("if the AI guesses wrong, the game will prompt you to tell the AI");
		System.out.println("what you were thinking of, and then to provide a question that would");
		System.out.println("distinguish it's wrong guess from what you were actually thinking of.");
		System.out.println();
		
		// Game control commands:
		System.out.println("GAME COMMANDS:");
		System.out.println("y  -  (in response to a question) Yes / (in response to an answer) Correct");
		System.out.println("n  -  (in response to a question) No / (in response to an answer) Incorrect");
		System.out.println("x  -  quit");
		System.out.println();
	}

}
