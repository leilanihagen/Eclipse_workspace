import java.util.concurrent.ThreadLocalRandom;

// Redraw all 9 JPanels every time game state changes...

/**
 * Main workhorse class of the tic tac toe game, Game stores all the relevant information about the
 * current game being played and provides handy, straightforward methods for modifying game states.
 * One instance of Game is used throughout a gameplay session to keep track of the user's
 * preferences that are set at the beginning of a session, information about the moves that have
 * been made by computer and user, as well as internal data such as whether the board is in
 * "enabled" or play mode and if users can give input to it.
 * 
 * @author Leilani Hagen
 * @date May 17, 2018
 *@assignment PA3 - CSE223
 *
 */
public class Game {

	// Constants:
	public static int X = 2;
	public static int O = 3;
	public static int USER = 4;
	public static int COMPUTER = 5;

	// Member fields:
	private int userSprite;
	private int computerSprite;
	private int[][] board; // Board data
	private String userNickname;
	
	// Flags:
	private boolean playMode;
	private boolean userStarts;
	private int lastTurn;
	private boolean onUserTurn;
	
	// Constructor:
	public Game() {
		/* Constructor, create a new gameplay instance! */
		
		board = new int[3][3];
		userSprite = X; // Set default value...
		computerSprite = O;
		playMode = false; // Default/start game NOT in playMode.
	}
	
	// userSprite:
	public int setUserSprite(int spriteCode) {
		/* Sets the user's "sprite"/player icon, and effectually defaults the computer to the
		   non-chosen sprite. Returns -1 if spriteCode invalid. */
		
		if (spriteCode == X || spriteCode == O) {
			userSprite = spriteCode;
			return 0; // Success.
		} else return -1; // Invalid spriteCode.
	}
	public int getUserSprite() {
		/* Read the current setting of the user's sprite. */
		
		return userSprite;
	}

	// playMode:
	public void setPlayMode(boolean on) {
		/* Sets Game instance to playMode, which starts the turns between user and computer and
		   allows editing of the Play Board, while simultaneously disabling editing of the New
		   Game Setup section which allows users to set preferences before the game starts. In
		   contrast, when playMode is off/false, the Play Board cannot be used and the preferences
		   section can be edited by the user. */
		
		if (on) {
			playMode = true;
		} else playMode = false;
	}
	public boolean inPlayMode() {
		/* Is the Game instance in playMode? */
		
		return playMode;
	}

	public boolean userStarts() {
		/* Does the user make the first move? */
		
		if (userStarts) {
			return true;
		} else return false;
	}
	
	// userNickname:
	public void setUserNickname(String userNickname) {
		/* Set the user's nickname. */
		
		this.userNickname = userNickname;
	}
	
	// lastTurn:
	public void setStartingPlayer(int player) {
		/* Initialize the value of private flag var lastTurn as if there was a turn preceding the
		   initial turn... */
		
		if (player == USER) {
			userStarts = true;
			lastTurn = COMPUTER;
			onUserTurn = false;
		} else {
			userStarts = false;
			lastTurn = USER;
			onUserTurn = true;
		}
	}

	// doUserMove():
	public int doUserMove(int row, int col) {
		/* Store the instance of a move in the correct entry in the board array based on row and
		   col of move, and record which user (X/O) made the move. Rows and columns index 0, 1,
		   2. Returns 1 if call attempts to overwrite existing turn data in the instance's board
		   array, -1 if spriteCode provided is invalid. */

//		onUserTurn = true;
		
		board[row][col] = userSprite;
		lastTurn = USER;

//		onUserTurn = false;
		lastTurn = USER;
		return 0; // Success.
	}

	// doComputerMove():
	public void doComputerMove() {
		/* The computer's strategy is to pick the middle square if it is available, otherwise
		   generate random row and col values until a free one is found, then take this. */
		
//		onUserTurn = false;

		int row = 1, col = 1;
		if (board[1][1] == 0) { // If middle is available...
			board[row][col] = computerSprite;
		}
		else {
			while (true) {
				row = ThreadLocalRandom.current().nextInt(0, 3);
				col = ThreadLocalRandom.current().nextInt(0, 3);
				if (board[row][col] == 0) {
					board[row][col] = computerSprite;
					break;
				}
			}
		}
		UI.simulateTurn(row, col);
//		onUserTurn = true;
		lastTurn = COMPUTER;
	}
	
	// simulate():
	public void simulate(int row, int col) throws InterruptedException {
		/* Combines calls to method doUserMove() and doComputerMovie to choose whether the user should
		   go next or the computer based on who went last, then simulates the correct move.. */
		
		if (lastTurn == USER) {
			this.doComputerMove();
			Thread.sleep(1000);
			lastTurn = COMPUTER;
		}
		else {
			this.doUserMove(row, col);
		}
	}

}
