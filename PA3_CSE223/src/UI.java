import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import java.awt.Panel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import java.awt.Insets;
import javax.swing.JSeparator;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.JMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * JFrame class  for the tic tac toe GUI. I got pretty stuck during the development of this. My
 * goal was to make a state-machine type of system using the Game class and UI, and I wrote various
 * methods and flag variables to keep track of game preferences data given by the user as well as
 * whose turn it currently is, the user's or the computer's. Specifically, I got stuck trying to
 * repaint the board after the computer's turn. I tried to accomplish this by using a series of
 * flag variables including the "r1c1Redraw" that is set to true when the computer chooses a
 * square.
 * 
 * The START button kicks the game into "playMode" which allows editing of the play board and
 * disallows editing of the preferences section. Additionally, the start button is supposed to
 * set the initial value of all the information that the user selects from the preferences window,
 * including who goes first, and this is supposed to kick off the round of turns.
 * 
 * I'm not entirely sure what I'm doing wrong, but I suspect it's multiple things and also that I
 * overcomplicated things with my approach. Feedback will be much appreciated!
 * 
 * @author Leilani Hagen
 * @date May 18, 2018
 * @assignment PA3 - CSE 223
 */
public class UI extends JFrame {

	// State variables:
	public boolean r1c1Redraw = false; // Static?
	public boolean r1c2Redraw = false;
	public boolean r1c3Redraw = false;
	public boolean r2c1Redraw = false;
	public boolean r2c2Redraw = false;
	public boolean r2c3Redraw = false;
	public boolean r3c1Redraw = false;
	public boolean r3c2Redraw = false;
	public boolean r3c3Redraw = false;

	private Game game; // INITIALIZE
	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField enterNickname;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI frame = new UI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UI() {
		
		// Game data:
		game = new Game(); // Create the user's profile...
		
		setTitle("Tic Tac Toe!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 300, 850, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] {75, 0, 0};
		gbl_contentPane.rowHeights = new int[] {75, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel boardTitle = new JPanel();
		boardTitle.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Play Board", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_boardTitle = new GridBagConstraints();
		gbc_boardTitle.insets = new Insets(0, 0, 0, 5);
		gbc_boardTitle.fill = GridBagConstraints.BOTH;
		gbc_boardTitle.gridx = 0;
		gbc_boardTitle.gridy = 0;
		contentPane.add(boardTitle, gbc_boardTitle);
		GridBagLayout gbl_boardTitle = new GridBagLayout();
		gbl_boardTitle.columnWidths = new int[]{75, 75, 75, 0};
		gbl_boardTitle.rowHeights = new int[]{75, 75, 75, 0};
		gbl_boardTitle.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_boardTitle.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		boardTitle.setLayout(gbl_boardTitle);
		
		// r1c1:
		boardField r1c1 = new boardField(game);
		r1c1.setCoords(0, 0, 70, 70);
		if (r1c1Redraw) {
			r1c1.setComponentActivated();
			repaint();
		}
		r1c1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("("+e.getX()+", "+e.getY()+")");
				if (game.inPlayMode()) {
					simulateTurn(0, 0);
					r1c1.setComponentActivated(); // Paint
					repaint();
				}
			}
		});
		GridBagConstraints gbc_r1c1 = new GridBagConstraints();
		gbc_r1c1.fill = GridBagConstraints.BOTH;
		gbc_r1c1.insets = new Insets(0, 0, 5, 5);
		gbc_r1c1.gridx = 0;
		gbc_r1c1.gridy = 0;
		boardTitle.add(r1c1, gbc_r1c1);
		r1c1.setBackground(Color.LIGHT_GRAY);
		
		// r1c2:
		boardField r1c2 = new boardField(game);
		if (r1c2Redraw) {
			r1c2.setComponentActivated();
			repaint();
		}
		r1c2.setBackground(Color.WHITE);
		GridBagConstraints gbc_r1c2 = new GridBagConstraints();
		gbc_r1c2.fill = GridBagConstraints.BOTH;
		gbc_r1c2.insets = new Insets(0, 0, 5, 5);
		gbc_r1c2.gridx = 1;
		gbc_r1c2.gridy = 0;
		boardTitle.add(r1c2, gbc_r1c2);
		r1c2.setCoords(0, 0, 70, 70);
		r1c2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("("+e.getX()+", "+e.getY()+")");
				if (game.inPlayMode()) {
					simulateTurn(0, 1);
					r1c2.setComponentActivated();
					repaint();
				}
			}
		});
		// r1c3:
		boardField r1c3 = new boardField(game);
		if (r1c3Redraw) {
			r1c3.setComponentActivated();
			repaint();
		}
		r1c3.setCoords(0, 0, 70, 70);
		r1c3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("("+e.getX()+", "+e.getY()+")");
				if (game.inPlayMode()) {
					simulateTurn(0, 2);
					r1c3.setComponentActivated();
					repaint();
				}
			}
		});
		r1c3.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_r1c3 = new GridBagConstraints();
		gbc_r1c3.fill = GridBagConstraints.BOTH;
		gbc_r1c3.insets = new Insets(0, 0, 5, 0);
		gbc_r1c3.gridx = 2;
		gbc_r1c3.gridy = 0;
		boardTitle.add(r1c3, gbc_r1c3);
				
		// r2c1:
		boardField r2c1 = new boardField(game);
		if (r2c1Redraw) {
			r2c1.setComponentActivated();
			repaint();
		}
		r2c1.setCoords(0, 0, 70, 70);
		r2c1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("("+e.getX()+", "+e.getY()+")");
				if (game.inPlayMode()) {
					simulateTurn(1, 0);
					r2c1.setComponentActivated();
					repaint();
				}
			}
		});
		r2c1.setBackground(Color.WHITE);
		GridBagConstraints gbc_r2c1 = new GridBagConstraints();
		gbc_r2c1.fill = GridBagConstraints.BOTH;
		gbc_r2c1.insets = new Insets(0, 0, 5, 5);
		gbc_r2c1.gridx = 0;
		gbc_r2c1.gridy = 1;
		boardTitle.add(r2c1, gbc_r2c1);
				
		// r2c2:
		boardField r2c2 = new boardField(game);
		if (r2c2Redraw) {
			r2c2.setComponentActivated();
			repaint();
		}
		r2c2.setCoords(0, 0, 70, 70);
		r2c2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("("+e.getX()+", "+e.getY()+")");
				if (game.inPlayMode()) {
					simulateTurn(1, 1);
					r2c2.setComponentActivated();
					repaint();
				}
			}
		});		
		r2c2.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_r2c2 = new GridBagConstraints();
		gbc_r2c2.fill = GridBagConstraints.BOTH;
		gbc_r2c2.insets = new Insets(0, 0, 5, 5);
		gbc_r2c2.gridx = 1;
		gbc_r2c2.gridy = 1;
		boardTitle.add(r2c2, gbc_r2c2);
				
		// r2c3:
		boardField r2c3 = new boardField(game);
		if (r2c3Redraw) {
			r2c3.setComponentActivated();
			repaint();
		}
		r2c3.setCoords(0, 0, 70, 70);
		r2c3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("("+e.getX()+", "+e.getY()+")");
				if (game.inPlayMode()) {
					simulateTurn(1, 2);
					r2c3.setComponentActivated();
					repaint();
				}
			}
		});
		r2c3.setBackground(Color.WHITE);
		GridBagConstraints gbc_r2c3 = new GridBagConstraints();
		gbc_r2c3.fill = GridBagConstraints.BOTH;
		gbc_r2c3.insets = new Insets(0, 0, 5, 0);
		gbc_r2c3.gridx = 2;
		gbc_r2c3.gridy = 1;
		boardTitle.add(r2c3, gbc_r2c3);
				
		// r3c1:
		boardField r3c1 = new boardField(game);
		if (r3c1Redraw) {
			r3c1.setComponentActivated();
			repaint();
		}
		GridBagConstraints gbc_r3c1 = new GridBagConstraints();
		gbc_r3c1.fill = GridBagConstraints.BOTH;
		gbc_r3c1.insets = new Insets(0, 0, 0, 5);
		gbc_r3c1.gridx = 0;
		gbc_r3c1.gridy = 2;
		boardTitle.add(r3c1, gbc_r3c1);
		r3c1.setCoords(0, 0, 70, 70);
		r3c1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("("+e.getX()+", "+e.getY()+")");
				if (game.inPlayMode()) {
					simulateTurn(2, 0);
					r3c1.setComponentActivated();
					repaint();
				}
			}
		});
		r3c1.setBackground(Color.LIGHT_GRAY);
		
		// r3c2:
		boardField r3c2 = new boardField(game);
		if (r3c2Redraw) {
			r3c2.setComponentActivated();
			repaint();
		}
		r3c2.setCoords(0, 0, 70, 70);
		r3c2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("("+e.getX()+", "+e.getY()+")");
				if (game.inPlayMode()) {
					simulateTurn(2, 1);
					r3c2.setComponentActivated();
					repaint();
				}
			}
		});
		r3c2.setBackground(Color.WHITE);
		GridBagConstraints gbc_r3c2 = new GridBagConstraints();
		gbc_r3c2.fill = GridBagConstraints.BOTH;
		gbc_r3c2.insets = new Insets(0, 0, 0, 5);
		gbc_r3c2.gridx = 1;
		gbc_r3c2.gridy = 2;
		boardTitle.add(r3c2, gbc_r3c2);
						
		// r3c3:
		boardField r3c3 = new boardField(game);
		if (r3c3Redraw) {
			r3c3.setComponentActivated();
			repaint();
		}
		r3c3.setCoords(0, 0, 70, 70);
		r3c3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("("+e.getX()+", "+e.getY()+")");
				if (game.inPlayMode()) {
					simulateTurn(2, 2);
					r3c3.setComponentActivated();
					repaint();
				}
			}
		});
		r3c3.setBackground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_r3c3 = new GridBagConstraints();
		gbc_r3c3.fill = GridBagConstraints.BOTH;
		gbc_r3c3.gridx = 2;
		gbc_r3c3.gridy = 2;
		boardTitle.add(r3c3, gbc_r3c3);
		
		JPanel menuPane = new JPanel();
		menuPane.setLayout(null);
		GridBagConstraints gbc_menuPane = new GridBagConstraints();
		gbc_menuPane.fill = GridBagConstraints.BOTH;
		gbc_menuPane.gridx = 1;
		gbc_menuPane.gridy = 0;
		contentPane.add(menuPane, gbc_menuPane);
		
		JLabel lblWouldYouLike = new JLabel("Play as X or O?");
		lblWouldYouLike.setBounds(16, 41, 105, 16);
		menuPane.add(lblWouldYouLike);
		
		JLabel lblNewGameSetup = new JLabel("New Game Setup");
		lblNewGameSetup.setFont(new Font("Lucida Grande", Font.BOLD, 14));
		lblNewGameSetup.setBounds(6, 6, 130, 23);
		menuPane.add(lblNewGameSetup);
		
		// Select X:
		JRadioButton radioButtonX = new JRadioButton("X");
		radioButtonX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!game.inPlayMode()) {
					game.setUserSprite(Game.X);
					System.out.println("X");
				} else {
					radioButtonX.setEnabled(false);
				}
			}
		});
		radioButtonX.setSelected(true);
		radioButtonX.setBounds(16, 60, 44, 23);
		menuPane.add(radioButtonX);

		// Select O:
		JRadioButton radioButtonO = new JRadioButton("O");
		radioButtonO.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!game.inPlayMode()) {
					game.setUserSprite(Game.O);
					System.out.println("O");
				} else {
					radioButtonO.setEnabled(false);
				}
			}
		});
		radioButtonO.setBounds(62, 60, 44, 23);
		menuPane.add(radioButtonO);
		
		// Group the X/O buttons:
		ButtonGroup XorO = new ButtonGroup();
		XorO.add(radioButtonX);
		XorO.add(radioButtonO);
		
		JLabel lblWhoGoesFirst = new JLabel("Enter your nickname:");
		lblWhoGoesFirst.setBounds(16, 95, 142, 16);
		menuPane.add(lblWhoGoesFirst);
		
		enterNickname = new JTextField();
		enterNickname.setBounds(16, 115, 130, 26);
		menuPane.add(enterNickname);
		if (!game.inPlayMode()) {
			enterNickname.setEditable(true);
			game.setUserNickname(enterNickname.getText());
		} else {
			enterNickname.setEnabled(false);
		}
		enterNickname.setColumns(10);
		
		JLabel lblFirstTurn = new JLabel("Who starts?");
		lblFirstTurn.setBounds(16, 153, 90, 16);
		menuPane.add(lblFirstTurn);
		
		JRadioButton rdbtnMe = new JRadioButton("Me");
		rdbtnMe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!game.inPlayMode()) {
					game.setStartingPlayer(Game.USER);
				} else {
					rdbtnMe.setEnabled(false);
				}
			}
		});
		rdbtnMe.setSelected(true);
		rdbtnMe.setBounds(16, 172, 141, 23);
		menuPane.add(rdbtnMe);
		
		JRadioButton rdbtnComputer = new JRadioButton("Computer");
		rdbtnMe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!game.inPlayMode()) {
					game.setStartingPlayer(Game.COMPUTER);
				} else {
					rdbtnMe.setEnabled(false);
				}

			}
		});
		rdbtnComputer.setBounds(16, 198, 141, 23);
		menuPane.add(rdbtnComputer);
		
		ButtonGroup meOrComp = new ButtonGroup();
		meOrComp.add(rdbtnMe);
		meOrComp.add(rdbtnComputer);
		
		JButton btnStart = new JButton("START");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnStart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				game.setPlayMode(true);
				if (!game.userStarts()) {
					try {
						game.simulate(0,0); // Dummy coordinates won't be used...
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				} // else: wait for a click event in one of the board JPanels...
			}
		});
		btnStart.setBackground(new Color(238, 238, 238));
		btnStart.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		btnStart.setBounds(475, 214, 117, 29);
		menuPane.add(btnStart);
		
	}

	public void simulateTurn(int row, int col) {
		/* Accepts 0-2 row and col values and redraws the component with the matching indices... */
		
		switch (row) {
		case 0 : 
			switch (col) {
			case 0: r1c1Redraw = true;
			break;
			case 1: r1c2Redraw = true;
			break;
			case 2: r1c3Redraw = true;
			break;
			}
			break;
		case 1 : 
			switch (col) {
			case 0: r2c1Redraw = true;
			break;
			case 1: r2c2Redraw = true;
			break;
			case 2: r2c3Redraw = true;
			break;
			}
			break;
		case 2 : 
			switch (col) {
			case 0: r3c1Redraw = true;
			break;
			case 1: r3c2Redraw = true;
			break;
			case 2: r3c3Redraw = true;
			break;
			}
			break;
		}
	}
}
