import java.awt.Graphics;

import javax.swing.JPanel;
/**
 * JPanel class where instances represent individual squares on the tic tac toe board, each handled
 * independently of one another. Class stores data about the Game instance associated with the
 * panel and the relevant coordinates of the panel for drawing the game sprites within the panels.
 * Will paint the correct sprite in the instance panel upon mouse-click when the panel is in
 * "componentActivated" mode.
 * 
 * @author Leilani Hagen
 * @date May 18, 2018
 * @assignment PA3 - CSE223
 *
 */
public class boardField extends JPanel {

	// State variables:
	private boolean componentActivated;
	
	// Member fields:
	private Game game;
	private int originX;
	private int originY;
	private int width;
	private int height;

	public boardField(Game game) {
		/* Minimal constructor... */

		this.game = game;
		componentActivated = false;
	}
	public void setCoords(int originX, int originY, int width, int height) {
		/* Set coordinates used to draw the sprites inside the component. */
		
		this.originX = originX;
		this.originY = originY;
		this.width = width;
		this.height = height;
	}
	public void setComponentActivated() {
		/* Toggle/set true componentActivated flag which tells the individual sprite to be
		   repainted when the screen is redrawn. */

		componentActivated = true;
	}

	public void paintComponent(Graphics g) {
		/* Extension of the paintComponent class to fill in the sprites upon turn completion in a
		   given board cell component. */

		super.paintComponent(g);
		if (componentActivated == true) {
			if (game.getUserSprite() == Game.X) {
				g.drawLine(originX, originY, width, height);
				g.drawLine(originX+width, originY, originX, originY+height);
			} else {
				g.drawOval(originX, originY, width, height);
			}
		}
	}

}
