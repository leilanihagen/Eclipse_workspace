/**
 * Typical linked list class with add, delete, traverse, etc. Implemented without sentinel.
 * @author Leilani Hagen
 * @date May 14, 2018
 *
 */
public class LL_NoSentinel {
	
	private Node handle; // First node or the list, will be maintained as the "start" of the list.
	private boolean firstAddFlag = true;
	
	public LL_NoSentinel() {
		handle = new Node();
	}
	
	public void add(int number) {
		/* Insert a number into the linked list. A node will be created to store number and it will
		   be inserted in the correct location based on size (smallest to largest). */
		
		// Edge case for populating the handle node (done upon first add()):
		if (firstAddFlag == true) { // Only true before first add()...
			handle.setData(number);
			firstAddFlag = false;
			return;
		}
		
		Node newNode = new Node(number); // Make a new node and store our number.
		
		Node prev = handle;
		Node curr = handle;
		while (curr != null) {
			if (curr.getData() >= number) {
				if (curr.equals(handle)) { // Edge case for replacing the handle...
					// newNode should replace handle (number is smallest in list):
					newNode.setNext(handle); // Link new node to handle.
					handle = newNode; // Replace the handle.
					return;
				} // else:
				// General insertion case:
				prev.setNext(newNode);
				newNode.setNext(curr);
				return;
			} // else:
			// Continue traversal:
			prev = curr;
			curr = curr.getNext();
		} // Traversal completed without finding a Node with data greater than number.
		// number is greatest in list, so append to the end:
		prev.setNext(newNode);
	}
	public void add(Node newNode) {
		/* Add a preexisting node... */
	}
	
	public void print() {
		/* Traverse the sorted list, printing each node as it is visited. */
		
		Node curr = handle;
		while (curr != null) {
			System.out.println(curr.getData());
			curr = curr.getNext();
		}
	}
	
	public boolean contains(int target) {
		/* Determine if list contains target value. Returns true if number is in list, else
		   returns false. */
		
		Node curr = handle;
		while (curr != null) {
			if (curr.getData() == target) {
				return true;
			} // else:
			curr = curr.getNext(); // Keep traversing...
		} // target never found:
		return false;
	}
	
	public void delete(int number) {
		/* Deletes the (first occurrence of?) node containing number, iff number exists in list... */
		
		if (!this.contains(number)) {
			return; // Do nothing...
		}
		
		// Traverse for address of node to delete:
		Node prev = handle;
		Node curr = handle;
		Node deleteNode = null;
		while (curr != null) {
			if (curr.getData() == number) {
				deleteNode = curr;
				break;
			}
			prev = curr;
			curr = curr.getNext();
		}
		
		// Edge case for deleting the handle:
		if (deleteNode.equals(handle)) {
			handle = handle.getNext(); // This does essentially the same thing as the gen. case,
			return; // but preserves the value of the handle for the instance...
		} // else:
		
		// General case delete (no edge case for deleting last node necessary...):
		prev.setNext(deleteNode.getNext()); // Skip over deleteNode.
	}
	
	public Node getHandle() {
		return handle;
	}
}
