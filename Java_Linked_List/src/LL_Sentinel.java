/**
 * Typical linked list class with add, delete, traverse, etc. Implemented using a no-data sentinel.
 * @author Leilani Hagen
 * @date May 14, 2018
 *
 */
public class LL_Sentinel {
	
	private Node handle; // First node or the list, will be maintained as the "start" of the list.
	
	public void add(int number) {
		/* Insert a number into the linked list. A node will be created to store number and it will
		   be inserted in the correct location based on size (smallest to largest). */
		
	}
	public void add(Node newNode) {
		/* Add a preexisting node... */
	}

}
