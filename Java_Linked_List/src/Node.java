/**
 * Bare-bones linked list Node for store integers.
 * @author Leilani Hagen
 * @date May 14, 2018
 *
 */
public class Node {
	
	private int data;
	private Node next;
	
	public Node(int data) {
		this.data = data;
	}
	public Node() {
	}
	
	public void setData(int data) {
		this.data = data;
	}
	public void setNext(Node next) {
		this.next = next;
	}
	
	public int getData() {
		return data;
	}
	public Node getNext() {
		return next;
	}

}
