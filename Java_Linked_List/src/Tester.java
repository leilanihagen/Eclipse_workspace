
public class Tester {
	
	public static void main(String[] args) {
		
		LL_NoSentinel ll = new LL_NoSentinel();
		
		// add():
		System.out.println("add() test:");
		System.out.println();
		ll.add(1);
		System.out.println("1 @ #1= "+ll.getHandle().getData());
		ll.add(0);
		System.out.println("0 @ #1= "+ll.getHandle().getData());
		System.out.println("1 @ #2="+ll.getHandle().getNext().getData());
		ll.add(400);
		System.out.println("400 @ #3="+ll.getHandle().getNext().getNext().getData());
		ll.add(-500);
		System.out.println("-500 @ #1="+ll.getHandle().getData());
		System.out.println("0 @ #2="+ll.getHandle().getNext().getData());
		System.out.println();
		
		// print():
		System.out.println("print() test:");
		System.out.println();
		ll.add(2);
		ll.add(3);
		ll.add(4);
		ll.print();
		System.out.println();
		
		// contains():
		System.out.println("contains() test:");
		System.out.println();
		boolean inList = ll.contains(-500);
		System.out.println("true="+inList);
		inList = ll.contains(-100000);
		System.out.println("false="+ inList);
		inList = ll.contains(0);
		System.out.println("true="+ inList);
		System.out.println();
		
		// delete():
		System.out.println("delete() test:");
		System.out.println();
		ll.delete(-500);
		System.out.println("list - (-500) =");
		ll.print();
		ll.delete(400);
		System.out.println("list - (400) =");
		ll.print();
		ll.delete(3);
		System.out.println("list - (3) =");
		ll.print();
		System.out.println();

	}

}
