package lab4;

import com.pi4.io.Gpio.*;
import java.uti.Scanner

public class Button {
	
	private int state=0;

	private void button() {
		
		GpioController gpio = GpioFactory.getInstance();
		GpioPinDigitalInput pin;
		pin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_04);
		
		while (true) {
			if (pin.getState() == PinState.HIGH) {
				state = 1;
			} else { state = 0; }
		}
		
	}
//	Gpio.pullUpDnControl(4, Gpio.PUD_UP);
}
