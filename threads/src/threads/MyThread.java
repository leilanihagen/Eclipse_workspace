package threads;

public class MyThread extends Thread {
	
	private String name;
	
	MyThread(String name){
		this.name = name;
	}
	
	public void run() {
		for (int i=0; i<100; i++) {
			System.out.print(this.name+": "+i+" ");
		}
		System.out.println();
	}

}
