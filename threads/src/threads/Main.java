package threads;

public class Main {

	public static void main(String[] args) {
		MyThread mt1 = new MyThread("bob");
		MyThread mt2 = new MyThread("sue");
		MyThread mt3 = new MyThread("mary");
		
		mt1.start();
		mt2.start();
		mt3.start();
		System.out.println("Main is exiting - goodbye!");
	}

}
