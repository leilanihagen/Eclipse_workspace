package src;

import java.awt.Color;
import java.awt.Dimension;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import com.google.protobuf.ExtensionRegistryLite;

import src.SimData.Shape;
import src.SimData.Snapshot;
import src.SimData.Simulation;

public class Main {
	
	private boolean setupMode = true;
	
	public Simulation buildInitialSimulation() {
		/*
		 * Builds the initial Simulation object using input from SetupPanel. Should be called in
		 * main()... 
		 */
		
		
	}
	
	private Shape buildShape() {
		/*
		 * Builds shape objects as they are created by the user. Should be called for every shape
		 * added by buildInitialSimulation().
		 */
		
		
	}

	public static Simulation buildSampleSimulation() {
		/*
		 * Builds complete sample ball objects, adds them to an ArrayList<>(), builds the
		 * initial snapshot, and adds this initial snapshot to the snapshotsBuilder.
		 * 
		 * *** Method should be replaced in the final version with buildInitialSimulation()! 
		 * 
		 * @return
		 */
		
		Shape.Builder ballBuilder = Shape.newBuilder()
				.setType("circle")
				.setPosX(100.0)
				.setPosY(100.0)
				.setSizeX(25.0)
				.setSizeY(25.0)
				.setMass(5.0);
		
		// Create initial ball 1 with ballBuilder defaults:
		Shape ball1 = ballBuilder.build();
		
		// Create 2 new ball objects using modified versions of ball1:
		Shape ball2 = ball1.toBuilder().setPosX(400).setPosY(140).setVelY(100.0).build();
		Shape ball3 = ball1.toBuilder().setPosX(450).setPosY(180).setVelX(-5.0).build();
		Shape ball4 = ball1.toBuilder().setPosX(300).setPosY(605).setVelX(-5.0).build();
		Shape ball5 = ball1.toBuilder().setPosX(500).setPosY(50).setVelX(-5.0).setVelY(-5.0).build();
		

		Shape ball6 = ball1.toBuilder().setPosX(0).setPosY(100).setVelY(100.0).setSizeX(17).setSizeY(17).build();
		Shape ball7 = ball1.toBuilder().setPosX(150).setPosY(180).setVelX(-5.0).setSizeX(17).setSizeY(17).build();
		Shape ball8 = ball1.toBuilder().setPosX(70).setPosY(200).setVelX(-5.0).setSizeX(17).setSizeY(17).build();
		Shape ball9 = ball1.toBuilder().setPosX(300).setPosY(500).setVelX(-5.0).setVelY(-5.0).setSizeX(10).setSizeY(10).build();
		Shape ball10 = ball1.toBuilder().setPosX(200).setPosY(700).setVelY(100.0).setSizeX(10).setSizeY(10).build();
		Shape ball11 = ball1.toBuilder().setPosX(700).setPosY(700).setVelX(-5.0).setSizeX(10).setSizeY(10).build();
		Shape ball12 = ball1.toBuilder().setPosX(80).setPosY(700).setVelX(-5.0).setSizeX(10).setSizeY(10).build();
		Shape ball13 = ball1.toBuilder().setPosX(800).setPosY(700).setVelX(-5.0).setVelY(-5.0).setSizeX(10).setSizeY(10).build();
		
		Shape ball14 = ball1.toBuilder().setPosX(500).setPosY(700).setVelY(-10.0).setVelX(1.0).setSizeX(60).setSizeY(60).build();
		Shape ball15 = ball1.toBuilder().setPosX(50).setPosY(300).setVelX(-5.0).setSizeX(15).setSizeY(15).build();
		
//		Shape ball16 = ball1.toBuilder().setPosX(600).setPosY(550).setVelX(-5.0).setSizeX(15).setSizeY(15).build();
//		Shape ball17 = ball1.toBuilder().setPosX(200).setPosY(500).setVelX(-5.0).setVelY(-5.0).setSizeX(15).setSizeY(15).build();

		// Add ball objects to an ArrayList<>():
		List<Shape> shapeList = new ArrayList<>();
		shapeList.add(ball1);
		shapeList.add(ball2);
		shapeList.add(ball3);
		shapeList.add(ball4);
		shapeList.add(ball5);
		shapeList.add(ball6);
		shapeList.add(ball7);
		shapeList.add(ball8);
		shapeList.add(ball9);
		shapeList.add(ball10);
		shapeList.add(ball11);
		shapeList.add(ball12);
		shapeList.add(ball13);
//		shapeList.add(ball14);
		shapeList.add(ball15);
//		shapeList.add(ball16);
//		shapeList.add(ball17);
		
		// Create first snapshot:
		Snapshot.Builder snapshot1Builder = Snapshot.newBuilder()
				.setTime(0.0)
				.addAllShapes(shapeList)
				.setGravity(-9.81);
		Snapshot snapshot1 = snapshot1Builder.build(); 
		
		// Build:
		Simulation.Builder snapshotsBuilder = Simulation.newBuilder()
				.addSnapshots(snapshot1);
		Simulation snapshots = snapshotsBuilder.build();
		
		return snapshots;
	}
	
//	/**
//	 * Creates a FileWriter object fw and writes initial snapshot information (for now,
//	 * later on we should have a list of all snapshots(?)) to specified filePath.
//	 * @param snapshots
//	 * @param filePath
//	 * @throws IOException
//	 */
//	public static void writeSnapshots(Simulation snapshots, String filePath)
//			throws IOException {
//		
//		// **Research method:
//		FileWriter fw = new FileWriter(filePath + ".ascii");
//		fw.write(snapshots.toString());
//		fw.close();
//		
//		// **Research method:
//		OutputStream os = new FileOutputStream(filePath + ".binary");
//		snapshots.writeTo(os);
//		os.close();
//	}
	
//	public static Simulation readSnapshots(String filePath)
//			throws IOException {
//
//		InputStream is = new FileInputStream(filePath + ".binary");
//		ExtensionRegistryLite eReg = ExtensionRegistryLite.getEmptyRegistry();
//		Simulation snapshots = Simulation.parseFrom(is, eReg);
//		is.close();
//
//		return snapshots;
//	}
	
	public static void main(String[] args) throws IOException {

		Simulation sample = buildSampleSimulation();
		//writeSnapshots(sample, "sample");
		//Simulation sampleRead = readSnapshots("sample");
		
		// Create and size the setup frame:
		JFrame setupFrame = new JFrame("Setup");
		setupFrame.setBounds(20, 150, 500, 300);
		setupFrame.setBackground(Color.blue);
		
		// Create and size the world frame:
		JFrame worldFrame = new JFrame("World");
		worldFrame.setBounds(540, 100, 715, 500);
		worldFrame.setBackground(Color.blue);
		
		// Quit the program upon setup frame close:
		setupFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create component to put in the setupFrame:
		SetupOptionsPanel setupPanel = new SetupOptionsPanel();
		setupPanel.setPreferredSize(setupFrame.getSize());
		setupPanel.setVisible(true);
		setupFrame.add(setupPanel);
		
		// Create component to put in the worldFrame:
		WorldSimPanel worldSimPanel = new WorldSimPanel(sample, 10); // Pay attention to the use of args here...
		worldSimPanel.setPreferredSize(worldFrame.getSize());
		worldSimPanel.setVisible(true);
		worldFrame.add(worldSimPanel);
				
		// Display frames:
		setupFrame.setVisible(true);
		worldFrame.setVisible(true);
	}
}