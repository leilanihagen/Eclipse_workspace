import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JProgressBar;

public class Foo extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JProgressBar progressBar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Foo frame = new Foo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Foo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JRadioButton rdbtnPressMe = new JRadioButton("press me");
		rdbtnPressMe.setBounds(38, 40, 141, 23);
		contentPane.add(rdbtnPressMe);
		
		table = new JTable();
		table.setBounds(91, 162, 1, 1);
		contentPane.add(table);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(132, 43, 146, 20);
		contentPane.add(progressBar);
	}
	public JProgressBar getProgressBar() {
		return progressBar;
	}
}
