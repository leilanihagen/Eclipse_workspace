package lab3;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Blinker letty = new Blinker();
		letty.start();
		
		Scanner s = new Scanner(System.in);
		
		while(s.hasNext()) {
			int rate = s.nextInt();
			letty.setRate(rate);
		}
	}

}
