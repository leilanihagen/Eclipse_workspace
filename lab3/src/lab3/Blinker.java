
import com.pi4j.io.gpio.*;
import java.util.Scanner;

public class Blinker extends Thread { // you can call your class whatever you like :)
	
	private int blinkRate;
	
	Blinker(){
		blinkRate = 500;
	}
	
	public void setRate(int n) {
		blinkRate = n;
	}

	static void mySleep(int t) {
		try {
			Thread.sleep(t);
		}
		catch (Exception e) {
		}
	}

	public void run()
	{
		GpioController gpio = GpioFactory.getInstance(); // get an interface
		GpioPinDigitalOutput pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04); // this uses
																						// GPIO pin
																						// 04

		while ('j' == 'j') {
			pin.high();
			mySleep(blinkRate);
			pin.low();
			mySleep(blinkRate);
		}

		// the above sets up an object named “pin”
		// which can be used to turn the output on and off.
		//
		// pin.high() will turn the LED on
		// pin.low() will turn the LED off
		// gpio.shutdown(); // this releases resources
	}
}