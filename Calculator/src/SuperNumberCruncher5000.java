import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.SwingConstants;

public class SuperNumberCruncher5000 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	// State of the machine:
	// 1: entering 1st number
	// 2: got '+'
	// 3: entering 2nd number
	// 4: got '='
	int state=1; // Default to s1

	int num1=0;
	private JTextField textField_1;
	private JTextField display;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SuperNumberCruncher5000 frame = new SuperNumberCruncher5000();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SuperNumberCruncher5000() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button_0 = new JButton("0");
		button_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_0.setBounds(67, 110, 45, 29);		
		contentPane.add(button_0);	

		JButton button_1 = new JButton("1");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_1.setBounds(124, 110, 45, 29);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("2");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_2.setBounds(181, 110, 45, 29);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("3");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_3.setBounds(67, 144, 45, 29);
		contentPane.add(button_3);
		
		JButton button_4 = new JButton("4");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_4.setBounds(124, 144, 45, 29);
		contentPane.add(button_4);
		
		JButton button_5 = new JButton("5");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_5.setBounds(181, 144, 45, 29);
		contentPane.add(button_5);
		
		JButton button_6 = new JButton("6");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_6.setBounds(67, 176, 45, 29);
		contentPane.add(button_6);
		
		JButton button_7 = new JButton("7");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_7.setBounds(124, 176, 45, 29);
		contentPane.add(button_7);
		
		JButton button_8 = new JButton("8");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_8.setBounds(181, 176, 45, 29);
		contentPane.add(button_8);
		
		JButton button_9 = new JButton("9");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_9.setBounds(67, 208, 45, 29);
		contentPane.add(button_9);
		
		JButton button_10 = new JButton("+");
		button_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_10.setBounds(124, 208, 45, 29);
		contentPane.add(button_10);
		
		JButton button_11 = new JButton("=");
		button_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doKey(e);
			}
		});
		button_11.setBounds(181, 208, 45, 29);
		contentPane.add(button_11);
		
		textField_1.setBounds(77, 18, 130, 26);
		textField_1 = new JTextField();
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		display = new JTextField();
		display.setHorizontalAlignment(SwingConstants.RIGHT);
		display.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		display.setText("0");
		display.setBounds(67, 58, 159, 40);
		contentPane.add(display);
		display.setColumns(10);
	}
	
	private void doKey(ActionEvent a) {
		JButton b = (JButton)a.getSource();
		String text = b.getText();
		boolean digit = "123456789".indexOf(text) >= 0; // See if char is digit...
		String current = display.getText();
//		System.out.println(text);
		
		// State 1: entering 1st number:
		if (state == 1) {
			if (text.equals("+") ||
			    text.equals("-") ||
			    text.equals("*") ||
			    text.equals("/")) {
				// Save the number:
				num1 = Integer.parseInt(current); // this is our first number :)
				state = 2;
				return; // we'll have to do some other stuff later...
			}
			if (digit) {
				if (current.equals("0")) {
					display.setText(text);
				}
				display.setText(current + text);
				return;
			} //  else:
			return; // illegal key(?)
		} // end of state 1

		// State 2: just got '+'
		if (state==2) {
			if (digit) {
				display.setText(text); // Display the digit just received
				state = 3;
				return;
			}
			return; // Remain in s2 and return
		}
		
		// State 3: entering 2nd number:
		if (state == 3 ) {
			if (text.equals("=")) {
				int num2 = Integer.parseInt(current);
				int result = num1+num2;
				display.setText(result + "");
				state = 4;
				return;
			}
			if (digit) {
				if (text.equals("0")) {
					display.setText(text);
				}
				else {
					display.setText(current + text);
				}
				return; // Remain in s3 to keep receiving digits and return.
			}
		}
		
		// State 4: just got '='
		if (state==4) { // (Only entered once answer has been displayed..)
			if (digit) {
				display.setText(text);
				state = 1;
				return;
			}
		}
		
		digit = false; // Refresh digit bool for next iteration
	}
	
//	private int doOp(int num1, int num2, String op) {
//		switch op(
//			case '+': return(num1 + num2);
//			case '-': return(num1 - num2);
//			case '*': return(num1 * num2);
//			case '/': return(num1 / num2);
//		);
//	}
}
